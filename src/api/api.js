import request from '@/utils/request'
//获取商品列表
export const getGoodsList = ({limit, count,page}) => {
  return request.get('store/good/query', {
    params: {
      storeId: 1,
      limit,
      count,
      page
    }
  })
}
//查询店铺信息
export const getStorehome = () => {
  return request.get('store/info/1', {
    params: {
    }
  })
}
//获取订单列表
export const getOrderList = ({limit,page ,start ,end} ) => {
  return request.get('store/order/list/1', {
    params: {
      page ,
      limit , 
      start ,
      end  
    }
  })
}
//获取商品详情
export const queryGoodDetails = ({id} ) => {
  return request.get('store/good/detail/', {
    params: {
     id 
    }
  })
}
//搜索订单
export const queryOrderDetails = ({mode,orderno,phone} ) => {
  return request.get('store/order/query/1', {
    params: {
     mode ,
     orderno,
     phone
    }
  })
}
//修改店铺
export const changeStore = ({data})=>{
  return request.put('store/info/update/1',{
    data
  })
}
//新增商品
export const addGoods = ({info}) =>{
  return request.post('store/good/create',{
    params:{
      storeId : 1,
      info
    }
  })
}