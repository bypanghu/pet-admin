import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/Layout'
import Home from '@/pages/Home'
import Goods from '@/pages/Goods'
import Orders from '@/pages/Orders'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'layout',
      component: Layout,
      redirect:'/home',
      children:[
        {
            path:'home',
            component:Home,
            meta: {title: '首页'}
          },
          {
            path:'orders',
            component:Orders,
            meta: {title: '订单'}
          },
          {
            path:'goods',
            component:Goods,
            meta: {title: '商品'}
          },
      ]
    }
  ]
})
